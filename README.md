# Tumor Detection
This supervised learning model detects malignant breast tumors using three 
supervised learning algorithms. The results show that logistic regression 
has the best performance for test and training data among the three algorithm.  